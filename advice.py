import requests
from lxml import etree

base_url = 'http://fucking-great-advice.ru'

def get_advice():
    r = requests.get(base_url)
    r.raise_for_status()
    tree = etree.HTML(r.text)
    advice = tree.xpath('/html//p[@id="advice"]')[0]
    return advice.text
