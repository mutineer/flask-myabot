import requests
import random
import logging
from lxml import etree

base_url = 'http://bash.im'
cache = {}

def get_random_quote():
    if not cache:
        logging.info('updating bash cache')
        r = requests.get(base_url + '/random?' + str(random.randint(1, 40000)))
        r.raise_for_status()
        tree = etree.HTML(r.text)
        quotes = tree.xpath('/html//div[@class="quote"]')
        for quote in quotes:
            try:
                url = base_url + quote.xpath('./div[@class="actions"]/a[@class="id"]')[0].get('href')
                text_el = quote.xpath('./div[@class="text"]')[0]
            except IndexError:
                continue

            text = text_el.text
            for ch in text_el:
                text += '\n' + ch.tail if ch.tail else ''
            cache[url] = text

    return cache.popitem()