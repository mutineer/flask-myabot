import requests
import copy

from config import bot_token, bot_name


def extract_result(r):
    r.raise_for_status()
    resp = r.json()
    if not resp['ok']:
        raise RequestFailed(resp['error_code'], resp['description'])
    return resp['result']

def get_updates(offset = None, limit = None, timeout = None):
    params = {'offset': offset, 'limit': limit, 'timeout': timeout}
    return extract_result( api.get(api.method_getUpdates, params) )

def send_message(text, chat_id, opts=None):
    params = copy.copy(opts) if isinstance(opts, dict) else {}
    params.update({'chat_id': chat_id, 'text': text})
    return extract_result( api.post(api.method_sendMessage, params) )
    
def send_sticker(sticker, chat_id):
    params = {}
    params.update({'chat_id': chat_id, 'sticker': sticker})
    return extract_result( api.post(api.method_sendSticker, params))

class FromJsonObj(object):
    @classmethod
    def from_json(cls, json):
        return cls(**json)


class Entity(FromJsonObj):
    def __init__(self, *, type, offset, length, **kwargs):
        self.type = type
        self.offset = offset
        self.length = length


class User(FromJsonObj):
    def __init__(self, *, id, first_name, last_name=None, username=None, type=None, **kwargs):
        self.id = id
        self.first_name = first_name


class GroupChat(FromJsonObj):
    def __init__(self, *, id, title, type=None, **kwargs):
        self.id = id
        self.title = title


class Chat(FromJsonObj):
    def __init__(self, **kwargs):
        try:
            self._inner = User.from_json(kwargs)
        except TypeError:
            self._inner = GroupChat.from_json(kwargs)

    def __getattr__(self, name):
        return getattr(self._inner, name)


class Message(FromJsonObj):
    def __init__(self, *, message_id, date, chat, text='', entities=[], **kwargs):
        self.id = message_id
        self.entities = [Entity.from_json(e) for e in entities]
        try:
            self.author = User.from_json(kwargs['from'])
        except KeyError:
            raise TypeError
        self.date = date
        self.chat = Chat.from_json(chat)
        self.text = text

    def get_command(self):
        self.entities.sort(key=lambda e: e.offset)
        command = next((e for e in self.entities if e.type == 'bot_command'), None)
        if not command:
            return None, None
        end = command.offset + command.length
        command_name = self.text[command.offset+1:end]
        if command_name.endswith(bot_name):
            command_name = command_name[:-len(bot_name)]
        return command_name, self.text[end+1:]


class Update(FromJsonObj):
    def __init__(self, *, update_id, message=None, **kwargs):
        self.id = update_id
        self.message = None
        if message:
            self.message = Message.from_json(message)


class RequestFailed(Exception):
    def __init__(self, code, description):
        self.code = code
        self.description = description

class api(object):
    main_url = 'https://api.telegram.org/bot'
    method_getMe = 'getMe'
    method_getUpdates = 'getUpdates'
    method_sendMessage = 'sendMessage'
    method_sendSticker = 'sendSticker'

    @staticmethod
    def url_for_method(method):
        return api.main_url + bot_token + '/' + method

    @staticmethod
    def get(method_name, params):
        url = api.url_for_method(method_name)
        return requests.get(url, params = params)

    @staticmethod
    def post(method_name, params):
        url = api.url_for_method(method_name)
        return requests.post(url, params = params)