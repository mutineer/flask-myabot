#!/bin/bash
hour=$(date -u '+%k')

if [[ ($hour -ge 7) && ($hour -le 18) ]]
then
    python3 ${OPENSHIFT_REPO_DIR}/sender.py
    num=$((1 + RANDOM % 10))
    echo number $num generated
    if [[ $num -le 2 ]]
    then
        python3 ${OPENSHIFT_REPO_DIR}/miu_sender.py
    fi
fi
