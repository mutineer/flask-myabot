import requests
from lxml import etree

video='https://twitter.com/gniloywest/status/902612139722067968'
doublePhoto='https://twitter.com/abroadunderhood/status/902653456812359681'
empty='https://twitter.com/varlamov/status/902790503917391873'

MEDIA_MESSAGES = {
    'AdaptiveMedia-video': 'Там видосик!',
    'AdaptiveMedia-singlePhoto': None,
    'AdaptiveMedia-doublePhoto': 'Две фоточки',
    'AdaptiveMedia-triplePhoto': 'Три фоточки',
    'AdaptiveMedia-quadPhoto': 'Четыре фоточки',
}

def get_media_message(link):
    r = requests.get(link)
    if r.status_code != 200:
        return None
    tree = etree.HTML(r.text)
    try:
        media = tree.xpath('//div[contains(@class,"permalink-tweet-container")]//div[@class="js-tweet-text-container"]/following-sibling::div//div[@class="AdaptiveMedia-container"]/div')[0]
    except IndexError:
        return None

    return MEDIA_MESSAGES.get(media.attrib.get('class'))