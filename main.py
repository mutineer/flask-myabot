import logging
import logging.handlers
import sys
import os
import re

from furl import furl

import advice
import bash
import twitter
from telegram_api import Update, send_message

VIDEO_RE = re.compile(r'.*facebook.*/videos/')

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

req_logger = logging.getLogger('bot_requests')
req_logger.propagate = False

rfh = logging.handlers.RotatingFileHandler('/home/nimvalar/nimvalar.pythonanywhere.com.requests.log', maxBytes=1024*1024*5, backupCount=2, encoding='utf-8')
rfh.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
req_logger.addHandler(rfh)
req_logger.setLevel(logging.INFO)

from flask import Flask, request
app = Flask(__name__)

@app.route('/token', methods=['GET', 'POST'])
def bot_entry():
    if request.method == 'POST':
        json = request.get_json(force=True)
        req_logger.info(json)
        update = Update.from_json(json)

        if update.message is None:
            return 'OK'

        command, args = update.message.get_command()
        if not command:
            for ent in update.message.entities:
                if ent.type == 'url':
                    ent_text = update.message.text[ent.offset:ent.offset + ent.length]
                    url = furl(ent_text)
                    if all((url.host == 'm.facebook.com',
                            url.path == '/story.php',
                            'story_fbid' in url.args,
                            'id' in url.args)):
                        new_url = 'https://facebook.com/{}/posts/{}'.format(
                            url.args['id'], url.args['story_fbid'])
                        send_message('Версия для десктопа:\n' + new_url,
                                     update.message.chat.id)

                    if VIDEO_RE.match(ent_text):
                        send_message('Там видосик!!', update.message.chat.id)

                    if url.origin == 'https://twitter.com':
                        msg = twitter.get_media_message(url.url)
                        if msg:
                            send_message(msg, update.message.chat.id,
                                         {'reply_to_message_id': update.message.id})

            app.logger.info("No command in message")
            return 'OK'

        app.logger.debug('Command parsed [%s, %s]', command, args)
        if command == 'echo':
            app.logger.info("Got echo command")
            if args:
                send_message(args, update.message.chat.id)
            else:
                app.logger.info("Got echo whithout parameter")

        elif command == 'bash':
            app.logger.info("Got bash command")
            try:
                url, text = bash.get_random_quote()
                send_message(text + '\n\n' + url, update.message.chat.id, {'disable_web_page_preview': True})
            except Exception:
                send_message("Sorry, some error:(", update.message.chat.id)

        elif command == 'advice':
            app.logger.info("Got advice command")
            try:
                advice_text = advice.get_advice()
                send_message('Вот тебе совет, блять:\n\n' + advice_text, update.message.chat.id)
            except Exception:
                app.logger.exception('Some error in advice')
                send_message("Sorry, some error:(", update.message.chat.id)

        elif command == 'start':
            app.logger.info("Got start command from ", update.message.author.first_name)
            send_message("So, let's start!", update.message.chat.id)
        else:
            app.logger.info("Got unknown message: " + update.message.text)
    return 'OK'

@app.route('/397956577')
def mya_bot_entry():
    if request.method == 'POST':
        pass

    return 'Hello from mya_bot'

@app.route('/')
def hello_world():
    return 'Hello World!'

if __name__ == '__main__':
    app.run(os.getenv('IP', '0.0.0.0'), os.getenv('PORT', '8080'))